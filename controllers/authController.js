const jwt = require("jsonwebtoken");
const cookieParser = require('cookie-parser');
const User = require("../models/User");
require("dotenv").config();

//handle errors
const handleErrors = (err) => {
  console.error(err.message, err.code);
  let errors = { email: "", psw: "" };

  //incorrect email error
  if(err.message === "incorrect email"){
    errors.email = 'That mail is not registred';
  }

  //incorrect psw error
  if(err.message === "incorrect password"){
    errors.psw = 'That password is incorrect';
  }

  //duplicate mail error
  if (err.code === 11000) {
    errors.email = "That email is already registred";
  }

  //validation errors
  if (err.message.includes("User validation failed")) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  }
  return errors;
};
//Creador del JWT para respuesta después del sign_in o log_in
const maxAge = 3*24*60*60;
const createToken = (id) => {
    return jwt.sign({id} , process.env.SECRET, {
        expiresIn: maxAge,
    });
};

module.exports.signup_get = (req, res) => {
  res.render("signup2");
};

module.exports.login_get = (req, res) => {
  res.render("login2");
};

module.exports.signup_post = async (req, res) => {
  const { name, email, psw } = req.body;
  console.log(req.body);
  // res.send('new signup');
  try {
    const user = await User.create({ name, email, psw });
    const token = createToken(user._id);
    res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge*1000});
    res.status(201).json({user: user._id});
  } catch (err) {
    console.log(err);
    const errors = handleErrors(err);
    res.status(400).send({ errors });
  }
};

module.exports.login_post = async (req, res) => {
  const { email, psw } = req.body;
  //console.log(email, psw);
  //res.send("user login");
  try {
      const user = await User.login(email, psw);
      const token = createToken(user._id);
      res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge*1000});
      res.status(200).json({user: user._id})
  } catch (err) {
      const errors = handleErrors(err);
      res.status(400).send({ errors });
  }
};

module.exports.logout_get = async (req, res) => {
    res.cookie('jwt', '', {maxAge: 1});
    res.redirect('/');
}

module.exports.integrantes_get = async (req, res) => {
  User.find().sort({ createdAt: -1 })
  .then(result => {
    res.render('integrantes', { users: result, title: 'Integrantes' });
  })
  .catch(err => {
    console.log(err);
  });
}