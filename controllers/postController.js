const Blog = require('../models/Blog');

const posteos_get = (req, res) => {
  Blog.find().sort({ createdAt: -1 })
    .then(result => {
      res.render('posts', { blogs: result, title: 'All posts' });
    })
    .catch(err => {
      console.log(err);
    });
}

const post_details = (req, res) => {
  const id = req.params.id;
  Blog.findById(id)
    .then(result => {
      res.render('details', { blog: result, title: 'Post Details' });
    })
    .catch(err => {
      console.log(err);
      res.render('404', { title: 'Post not found' });
    });
}

const create_get = (req, res) => {
  res.render('create', { title: 'Create a new Post' });
}

const create_post = (req, res) => {
  const blog = new Blog(req.body);
  //console.log(req);
  blog.save()
    .then(result => {
      res.redirect('/posteos');
    })
    .catch(err => {
      console.log(err);
    });
}

const post_delete = (req, res) => {
  const id = req.params.id;
  Blog.findByIdAndDelete(id)
    .then(result => {
      res.json({ redirect: '/posteos' });
    })
    .catch(err => {
      console.log(err);
    });
}

module.exports = {
  posteos_get,
  post_details,
  create_get,
  create_post,
  post_delete
}