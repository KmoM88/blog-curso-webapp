const express = require('express');
const mongoose = require('mongoose');
var cors = require('cors');
require('dotenv').config()
const authRoutes = require('./routes/authRoutes');
const {requireAuth, checkUser} = require('./middleware/authMiddleware');
const cookieParser = require('cookie-parser');
const { getRounds } = require('bcrypt');

const app = express();

// middleware
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cookieParser());
app.use(cors());

// view engine
app.set('view engine', 'ejs');

// database connection
const dbURI = process.env.DIR_DB_USR;
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(3000))
  .catch((err) => console.log(err));

// routes
app.get('*', checkUser);
app.get('/', (req, res) => res.render('home2'));
app.use(authRoutes);

// 404 page
app.use((req, res) => {
  res.status(404).render('404', { title: '404' });
});

// app.get('/set-cookies', (req, res) => {
//   res.cookie('newUser', false);
//   res.cookie('isEployee', true, {maxAge: 1000*60*60*24, httpOnly: true});
//   res.send('You got the coockies');
// });

// app.get('/read-cookies', (req, res) => {
//   const cookies = req.cookies;
//   console.log(cookies.newUser);
//   res.json(cookies.isEployee);
// });