const express = require('express');
const authController = require('../controllers/authController');
const postController = require('../controllers/postController');
const Blog = require('../models/Blog');
const User = require("../models/User");

const router = express.Router();

router.get('/signup2',authController.signup_get);
router.get('/login2',authController.login_get);
router.post('/signup2',authController.signup_post);
router.post('/login2',authController.login_post);
router.get('/logout',authController.logout_get);
router.get('/integrantes',authController.integrantes_get);
//router.get('/perfil',authController.integrantes_get);

router.get('/posteos',postController.posteos_get);
router.post('/posteos',postController.create_post);
router.get('/create',postController.create_get);
router.get('/posteos/:id', postController.post_details);
router.delete('/posteos/:id', postController.post_delete);




module.exports = router;